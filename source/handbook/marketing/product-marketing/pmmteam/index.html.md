---
layout: handbook-page-toc
title: "Product Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product marketing at GitLab

The product marketing team at GitLab is responsible for product positioning,  value messaging, and go-to-market strategy in order to support sales and outbound messaging with analysts, press, and prospects as well as facilitating inbound market data input into the product roadmap.

There are two equally important sides of product marketing at GitLab. First, is understanding the enterprise development team challenges and communicating how GitLab addresses specific their challenges and [use cases](/handbook/use-cases). Second, is a focus on the value and capabilities of the 10 DevOps stages and categories that GitLab enables.

In general, product marketing
1. Develops value based messaging and positioning to support GitLab sales and marketing
1. Enables sales with content and collateral
1. Supports field and digital campaigns with content, webinars, presentations, and strategic input.

### Use cases  
[Customer 'use cases'](/handbook/use-cases/) are a customer problem or initiative that needs a solution and attracts budget, typically defined In customer terms. In Product Marketing, we build content and messaging that engages prospects who are looking for solutions to specific challenges they face. In product marketing, we:
1. Research and prioritize customer 'use cases'.
1. Lead analyst reports and contribute to analyst research.
1. Define the ['buyer's journey'](/handbook/marketing/product-marketing/usecase-gtm) for a specific use case.
1. Audit our existing content and collateral for each stage of the buyer's journey.
1. Prioritize and refine existing or create new content.
1. Collaborate with the rest of the marketing groups (Marketing Program Management, Content Marketing, Digital Marketing, SDRs, etc.) to promote and measure GTM effectiveness.

### Stages and categories
DevOps stages and categories organize and define how we plan and engineer new GitLab features.   
1. Contribute to the stage and category vision.
1. Lead messaging for release posts and support feature descriptions.
1. Competitive research and comparisons.


The [Product Marketing - Overview Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1074672) is where we plan, manage, and track our work.

### Organization

Customer use cases are our primary focus, as we strive to communicate the value of GitLab and how it helps customers address specific challenges they are facing.  All of the GitLab stages and categories support various use cases in different degrees.  By focusing on Use Cases, we intend to remain connected to the market and our customers while educating them about how GitLab can solve their specific challenges.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-lboi{border-color:inherit;text-align:left;vertical-align:middle}
.tg .tg-cly1{background-color:#ffcc67;text-align:left;vertical-align:middle}
.tg .tg-cly2{background-color:#9698ed;text-align:left;vertical-align:middle}
.tg .tg-0lax{text-align:left;vertical-align:top}
.tg .tg-0lax2{height:15px;text-align:left;vertical-align:top}
@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
<div class="tg-wrap"><table class="tg" style="undefined;table-layout: fixed; width: 562px">
<colgroup>
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 80px">
<col style="width: 105px">
</colgroup>
  <tr>
    <td height="4" class="tg-xkfo" colspan="1"></td>
    <td height="4" class="tg-xkfo" colspan="8">UseCase</td>
    <td height="4" class="tg-xkfo" colspan="1">Industry/ Vertical</td>
  </tr>
  <tr>
    <th class="tg-lboi">Sr. PMM</th>

    <th class="tg-cly1">Agile</th>
    <th class="tg-cly1">SCM</th>
    <th class="tg-cly1">CI</th>
    <th class="tg-cly1">CD &amp; Release</th>
    <th class="tg-cly1">Dev Sec Ops</th>
    <th class="tg-cly1">E2E DevOps (Simplify DevOps)</th>
    <th class="tg-cly1">Cloud Native</th>
    <th class="tg-cly1">IaC</th>
    <th class="tg-cly1">Public Sector/ Regulated Industries</th>
  </tr>
  <tr>
    <td  height="10" class="tg-lboi">PMM</td>
    <td  height="10" class="tg-cly2" colspan="2">PMM</td>
    <td  height="10" class="tg-cly2" colspan="2">PMM</td>
    <td  height="10" class="tg-cly2" colspan="4">PMM</td>
    <td  height="10" class="tg-0lax"></td>
  </tr>
  <tr>
    <td height="1" class="tg-xkfo" colspan="10"></td>
  </tr>
  <tr>
    <td height="15" class="tg-0lax">PM section alignment</td>
    <td height="15" class="tg-0lax" colspan="2">Dev <br>(Eric)</td>
    <td height="15" class="tg-0lax" colspan="2">CI/CD <br>(Jason)</td>
    <td height="15" class="tg-0lax" colspan="1">Secure/ Defend <br>(David)</td>
    <td height="15" class="tg-0lax" colspan="3">Ops <br>(Kenny)</td>
    <td height="15" class="tg-0lax"></td>
  </tr>
</table></div>

In this model, Senior PMMs are responsible for both the collateral and messaging supporting a specific Use Case **and** also one or several stages. Detail stable counterparts assignments between product marketing and specific product groups is maintained in the [Product Categories page](/handbook/product/categories/#dev-section). PMMs support the team with messaging and go to market efforts, leading research, writing, and collateral development.

This model helps to define several stable counterpart relationships with the product and engineering teams, where specific use cases map to current sections in our hierarchy.


##### Key links
- [Messaging](/handbook/marketing/product-marketing/messaging/)
- [GitLab positioning](/handbook/positioning-faq/)
- [Hidden IT Groups](/handbook/marketing/product-marketing/it-groups/)
- [Defining GitLab roles and personas](/handbook/marketing/product-marketing/roles-personas/)
- [GitLab tiers](/handbook/marketing/product-marketing/tiers/)
- [Market segmentation - Industry verticals](/handbook/marketing/product-marketing/market-segmentation/)
- [Use Case - Go to market](/handbook/marketing/product-marketing/usecase-gtm)


#### Release vs. launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then release it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

### Team Structure

### Which product marketing manager should I contact?

- Listed below are areas of responsibility within the product marketing team:
 - **Agile Project Management** : Need a better way to manage projects using Agile methodology - [Cormac](/company/team/#cfoster3) and [Brian](/company/team/#brianglanz)
 - **Source Code Management (SCM)** : Create, manage and protect my source code - [John](/company/team/#j_jeremiah) and [Brian](/company/team/#brianglanz)
 - **Continuous Integration (CI)** : Increase the quality of my code while decreasing time to delivery -  [William](/company/team/#thewilliamchia) and [tbh1]()
 - **Continuous Delivery (CD)** : Speed up my build and release process and empower my developers to automatically deploy code  - [William](/company/team/#thewilliamchia) [tbh1]()
 - **Shift Left Security or DevSecOps** : Test for application security vulnerabilities early in my app dev lifecycle - [Cindy](/company/team/#cblake2000) [tbh2]()
 - **End to End DevOps** : I want to achieve expected results of DevOps given siloed teams, lack of visibility and collaboration which inhibits my speed of delivery  [Saumya](/company/team/#supadhyaya) and [tbh2]()
 - **Cloud Native** : Want to use more modern, cloud-native approaches to application development - [William](/company/team/#thewilliamchia)
 - **Infrastructure as Code** : Want to automatically provision, administer and maintain infrastructure as code - [William](/company/team/#thewilliamchia)
 - **Regulated Industries/Public Sector** -  [Traci](/company/team/#tracirobinsonwm), Senior PMM, Regulated Industries

#### PM / Product Section alignment
  - [**Dev Product Section**](/handbook/product/categories/#dev-section) - [John](/company/team/#j_jeremiah) AND [Cormac](/company/team/#cfoster3)
  - [**CI/CD**](/handbook/product/categories/#cicd-section) - [William](/company/team/#thewilliamchia)
  - [**Ops**](/handbook/product/categories/#ops-section) - [William](/company/team/#thewilliamchia) AND [Saumya](/company/team/#supadhyaya)
  - [**Secure**](/handbook/product/categories/#secure-section)  - [Cindy](/company/team/#cblake2000)
  - [**Defend**](/handbook/product/categories/#defend-section) - [Cindy](/company/team/#cblake2000)

  - [Ashish](/company/team/#kuthiala), Sr Director Strategic Marketing
