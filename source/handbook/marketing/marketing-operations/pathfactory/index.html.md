---
layout: handbook-page-toc
title: "PathFactory"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Uses  
PathFactory is leveraged as our content library & content distribution system. The [Digital Marketing team](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/) is primarily responsible for all the of the content within the PathFactory tool. To request the creation of a track or need to leverage PathFactory in a marketing intiative, please see the [instructions provided by the Digital Marketing team](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/pathfactory-management/#using-pathfactory). 


### Access   
The Marketing Operations team is responsible for managing access requests & provisioning the appropriate level of access for each role/function. Providing access is generally hanlded via baseline entitlement; however, if you need access and it is *not* part of your roles baseline entitlement please open a [`Single Person Access Request` issue](/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request) and provide business reason why access is needed.   

#### User Roles   
There are three levels of access - Admin, Author, Reporter - which are granted based on the persons' role and team function within GitLab. **All access levels** have the ability to view the analtyics page within the tool. 

##### Admin  
Admin access is primarily granted to Marketing Operations only; for PathFactory an `Admin` seat has been granted to the **Digital Marketing Program Manager** because this role is reponsible for creating, curating and managing all tags/tracks within PathFactory.  

##### Author 
`Author` access allows user to build, edit and publish content tracks applying existing tags to the assets. This level of access is granted to the Marketing Program Managers and select Content team members.  

##### Reporter 
`Reporter` access provides general visibility to content within PathFactory but does not allow end user to create or modify any of the content, tracks or tags. This level of access is granted for the general GitLab team member both within Marketing and elsewhere who have a business need to have access.  


### Listening Campaigns 
In Marketo, we have programs built to "listen" for PathFactory (PF) activity & content consumption allowing us to track behaviour without having to physically gate all the assets which disrupts the user experience.   

The PF<>Marketo listening programs are built to triggered based on the `slug` associated to each of asset. **Very Important: Do NOT to change the `slug` in PF without notifying Ops *prior* to making the change**. Each listening campaign has a Salesforce (SFDC) campaign associated to it tracking consumption and applying Bizible touchpoints.  

The naming convention for each of the listeners is specific to the asset type & is used as a trigger to the appropriate scoring campaign *within Marketo* at this time these listening campaigns have **no impact** on PathFactory engagement scores. The same naming convention is used for **both** Marketo & Salesforce campaigns.   

| Asset Type | Naming Convention |
| :--------- | :---------------- |
| Whitepaper | PF - Whitepaper - | 
| eBook      | PF - eBook -      | 
| Analyst Report | PF - Analyst Report - | 
| Research Report | PF - Research Report - | 
| Demo  | PF - Demo - | 
| Datasheet | PF - Datasheet - |   

#### Salesforce Campaign Type  

For the PathFactory listening campaigns there is a corresponding Salesforce `Campaign Type` to be used. The `Campaign Member Status` simply tracks if the content was consumed. This Salesforce `Campaign Type` should be used for **nothing** else. For greater details, see the [Business Ops Resources - Campaign details](/handbook/business-ops/resources/#campaign-type--progression-status).